package main;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
////----------------------------------------------------------------------------------------------------------------------
//        int[] arr1 = new int[10];
//
//        for (int i = 0; i < arr1.length; i++) {
//            arr1[i] = ((int) (Math.random() * 100) - 50);
//        }
////----------------------------------------------------------------------------------------------------------------------
//        MyArrays myArrays = new MyArrays();
//
//        int[] a = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//        myArrays.setArr(a);
//        myArrays.print();
//        myArrays.add(7, 4); myArrays.print();
//        myArrays.del(4); myArrays.print();
//        myArrays.addStart(8); myArrays.print();
//        myArrays.addEnd(2); myArrays.print();
//        myArrays.delStart(); myArrays.print();
//        myArrays.delEnd(); myArrays.print();
//        myArrays.addArray(arr1); myArrays.print();
//        myArrays.changeHalf(); myArrays.print();
//        myArrays.sortMinToMax(); myArrays.print();
//        myArrays.sortMaxToMin(); myArrays.print();
//        myArrays.reverse(); myArrays.print();
//        System.out.println(Arrays.toString(myArrays.getArray()));
//        System.out.println(myArrays.intoString());
//        myArrays.clear(); myArrays.print();
////----------------------------------------------------------------------------------------------------------------------
//        System.out.println("-----------------------------------------------------------------------------------------");
////----------------------------------------------------------------------------------------------------------------------
//        MyDyArrays myDyArrays = new MyDyArrays();
//
//        int[] b = {};
//        myDyArrays.setArr(b);
//        myDyArrays.print();
//        myDyArrays.add(7, 4); myDyArrays.print();
//        myDyArrays.del(4); myDyArrays.print();
//        myDyArrays.addStart(6); myDyArrays.print();
//        myDyArrays.addEnd(9); myDyArrays.print();
//        myDyArrays.delStart(); myDyArrays.print();
//        myDyArrays.delEnd(); myDyArrays.print();
//        myDyArrays.addArray(arr1);
//        myDyArrays.sortMaxToMin(); myDyArrays.print();
//        myDyArrays.sortMinToMax(); myDyArrays.print();
//        myDyArrays.reverse(); myDyArrays.print();
//        myDyArrays.changeHalf(); myDyArrays.print();
//        System.out.println(Arrays.toString(myDyArrays.getArray()));
//        System.out.println(myDyArrays.intoString());
//        myDyArrays.clear(); myDyArrays.print();
////----------------------------------------------------------------------------------------------------------------------
//        System.out.println("-----------------------------------------------------------------------------------------");
////----------------------------------------------------------------------------------------------------------------------
//        ArrayPerson arrayPerson = new ArrayPerson();
//
//        Person[] arr2 = {new Person(1, "Kate", 54),
//                        new Person(2,"Max", 41),
//                        new Person(3, "Ann", 32),
//                        new Person(4, "Lindsey", 24),
//                        new Person(5, "Chris", 45)};
//
//        arrayPerson.print();
//        arrayPerson.addArray(arr2); arrayPerson.print();
//        arrayPerson.add(new Person(18, "Jose", 34)); arrayPerson.print();
//        arrayPerson.del(1); arrayPerson.print();
//        arrayPerson.addStart(new Person(1, "John", 17)); arrayPerson.print();
//        arrayPerson.addEnd(new Person(7, "Marry", 25)); arrayPerson.print();
//        arrayPerson.delStart(); arrayPerson.print();
//        arrayPerson.delEnd(); arrayPerson.print();
//        System.out.println(Arrays.toString(arrayPerson.getArray()));
//        System.out.println(arrayPerson.intoString());
//        arrayPerson.printWrestr();
//        arrayPerson.clear(); arrayPerson.print();

        ArrayFactory factory = new ArrayFactory();

        ArraysInterface list = factory.getArrays(1);
        list.addStart(5);
        list.print();
    }
}

