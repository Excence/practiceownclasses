package main;

public class MyDyArrays extends BasicArray implements ArraysInterface {

    public void add(int a, int ind) {
        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        int[] buf = new int[arr.length + 1];

        if (arr.length == 0){ // если массив пустой
            buf[0] = a;
            arr = buf;
            return;
        }

        if (ind > arr.length){ // если указаный индекс больше размера массива
            for (int i = 0; i < arr.length; i++) {
                buf[i] = arr[i];
            }
            buf[buf.length-1] = a;
            arr = buf;
            return;
        }

        for (int i = 0; i < arr.length; i++) { // классический вариант
            if (i < ind) {
                buf[i] = arr[i];
            }
            if (i > ind) {
                buf[i + 1] = arr[i];
            }
        }
        buf[ind] = a;
        arr = buf;
    }

    public void del(int ind) {

        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        if (arr.length == 0){
            System.out.println("Удаление элемента невозможно: массив пустой");
            return;
        }

        if (ind > arr.length){
            System.out.println("Удаление элемента невозможно: в массиве меньше элементов");
            return;
        }

        int[] buf = new int[arr.length - 1];

        for (int i = 0; i < arr.length; i++) {
            if (i < ind){
                buf[i] = arr[i];
            }
            if (i > ind){
                buf[i - 1] = arr[i];
            }
        }
        arr = buf;
    }


    public void addStart(int a) {
        int[] buf = new int[arr.length + 1];
        buf[0] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i + 1] = arr[i];
        }
        arr = buf;
    }

    public void addEnd(int a) {
        int[] buf = new int[arr.length + 1];
        buf[buf.length - 1] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public void delStart() {
        int[] buf = new int[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i + 1];
        }
        arr = buf;
    }

    public void delEnd() {
        int[] buf = new int[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public void clear() {
        int[] buf = {};
        arr = buf;
    }

    public int[] getArray() {
        return arr;
    }

    public String intoString() {
        String str = new String();
        if(arr.length != 0){
            str += "[";
            for (int i = 0; i < arr.length-1; i++) {
                str += arr[i] + ", ";
            }
            str += arr[arr.length-1] + "]";
        } else {
            str = "Массив пустой";
        }
        return str;
    }

    public void print() {
        if (arr.length != 0) {
        System.out.print("[");
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println(arr[arr.length - 1] + "]");
    } else {
        System.out.println("Массив пустой");
    }
}

    public void addArray(int[] inn) {

        if (arr.length == 0) {
            arr = inn;
            return;
        }

        int x = arr.length;

        int[] buf = new int[arr.length+inn.length];
        for (int i = 0; i < buf.length; i++) {
            if (i < x) {
                buf[i] = arr[i];
                }
            if (i> x){
                buf[i] = inn[i-x];
            }
            arr = buf;
        }
    }
}

