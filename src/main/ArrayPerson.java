package main;

public class ArrayPerson  {

    Person[] arr = {};

    public void add(Person person){
        if (person.getId()<0){
            System.out.println("Введен отрицательный ID");
            return;
        }

        for (Person p: arr) {
            if (person.getId() == p.getId()){
                p.setAge(person.getAge());
                p.setName(person.getName());
                p.setId(person.getId());
                return;
            }
        }
        addEnd(person);
    }

    public void addArray(Person[] person) {
        if (arr.length == 0) {
            arr = person;
            return;
        }

        int x = arr.length;

        Person[] buf = new Person[arr.length + person.length];
        for (int i = 0; i < buf.length; i++) {
            if (i < x) {
                buf[i] = arr[i];
            }
            if (i > x) {
                buf[i] = person[i - x];
            }
            arr = buf;
        }
    }

    public void addStart(Person person){
        if (person.getId()<0){
            System.out.println("Введен отрицательный ID");
            return;
        }

        if (checkIfExists(person.getId())){
            return;
        }

        Person[] buf = new Person[arr.length + 1];
        buf[0] = person;
        for (int i = 0; i < arr.length; i++) {
            buf[i + 1] = arr[i];
        }
        arr = buf;
    }

    public void addEnd(Person person){
        if (person.getId()<0){
            System.out.println("Введен отрицательный ID");
            return;
        }

        if (checkIfExists(person.getId())){
            return;
        }

        Person[] buf = new Person[arr.length + 1];
        buf[buf.length - 1] = person;
        for (int i = 0; i < arr.length; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public void del(int id){

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].getId() == id){
                Person[] buf = new Person[arr.length-1];
                for (int j = 0; j < arr.length; j++) {
                    if (j < i){
                        buf[j] = arr[j];
                    }
                    if (j > i){
                        buf[j - 1] = arr[j];
                    }
                }
                arr = buf;
                return;
            } else {
                System.out.println("Нет элемента с таким индексом");
            }
        }
    }

    public void clear(){
        Person[] buf = {};
        arr = buf;
    }

    public void delStart(){
        Person[] buf = new Person[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i + 1];
        }
        arr = buf;
    }

    public void delEnd(){
        Person[] buf = new Person[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public String intoString(){
        String str = new String();
        if (arr.length > 0){
            str += "[";
            for (int i = 0; i < arr.length-1; i++) {
                str += arr[i].intoString() + ", ";
            }
            str += arr[arr.length-1] + "]";
        } else {
            str = "Массив пустой";
        }
        return str;
    }

    public void print(){
        if (arr.length > 0) {
            System.out.print("[");
            for (int i = 0; i < arr.length - 1; i++) {
                System.out.print(arr[i].intoString() + ", ");
            }
            System.out.println(arr[arr.length - 1].intoString() + "]");
        } else {
            System.out.println("Массив пустой");
        }
    }

    public Person[] getArray(){
        return arr;
    }

    private boolean checkIfExists(int id){
        for (Person p : arr) {
            if (p.getId() == id){
                System.out.println("Элемент с таким id уже существует");
                return true;
            }
        }
        return false;
    }

    public void printWrestr() {
        for (Person p : arr) {
            int i = p.getId();
            int a = p.getAge();
            int s = p.getName().length();
            if ((i >= 0) && (i <= 10) && (a >= 18) && (a <= 25) && (s > 3)) {
                System.out.println(p.intoString());
            }
        }
    }
}
