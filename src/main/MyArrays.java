package main;

public class MyArrays extends BasicArray implements ArraysInterface {

    protected  int[] arr = new int[10];

    public void add(int a, int ind) {
        arr[ind] = a;
    }

    public void del(int ind) {
        arr[ind] = 0;
    }

    public void addStart(int a) {
        arr[0] = a;
    }

    public void addEnd(int a) {
        arr[9] = a;
    }

    public void delStart() {
        arr[0] = 0;
    }

    public void delEnd() {
        arr[9] = 0;
    }

    public void clear() {
        for (int i = 0; i < 10; i++) {
            arr[i] = 0;
        }
    }

    public int[] getArray() {
        return arr;
    }

    public String intoString() {
        String str = new String();
        str += "[";
        for (int i = 0; i < 9; i++) {
            str +=  arr[i] + ", ";
        }
        str += arr[9] + "]";
        return str;
    }

    public void print() {
        System.out.print("[");
        for (int i = 0; i < 9; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println(arr[9] + "]");
    }

    public void addArray(int[] inn) {
        if (inn.length != 10) {
            System.out.println("Передаваемый массив не соответствует размеру заданного (10)");
        } else {
            for (int i = 0; i < 10; i++) {
                arr[i] = inn[i];
            }
        }
    }
}
