package main;

public class ArrayFactory {
    public ArraysInterface getArrays(int a){
        switch (a){
            case 1 : return new MyArrays();
            case 2 : return new MyDyArrays();
            default: return null;
        }

    }
}
